set -eux
mkdir seer && cd seer
filename=$(basename $1)
awk 'BEGIN {OFS="\t"}{if($2=="0" || $2=="1" || $2=="pheno"){print $1, $2}}' $1 > phenotypes.txt
pyseer --kmers ../fsmlite/fsm_kmers.txt.gz --phenotypes phenotypes.txt --distances ../mash/all_distances.tsv --cpu 8 --lrt-pvalue 0.05 > seer_out_on_${filename}
cd ..
