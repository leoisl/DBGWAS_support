#usage:
#bash pipeline.sh <DBGWAS_strains_file>
source /data2/leandro/GWAS/PLOS_review/comparison/SEER/venv/bin/activate

set -eux

#get the filename
filename=$(basename $1)

mkdir $filename && cd $filename

#Running fsmlite
/usr/bin/time --verbose -o fsmlite_${filename}.perf bash ../run_fsmlite.sh $1

#Running mash
/usr/bin/time --verbose -o mash_${filename}.perf bash ../run_mash.sh $1

#Running pyseer
/usr/bin/time --verbose -o pyseer_${filename}.perf bash ../run_seer.sh $1

cd ..

deactivate
