set -eux
mkdir fsmlite && cd fsmlite
awk '{if($2=="0" || $2=="1"){print $1, $3}}' $1 > fsm_files.txt

#The -s and -S options control the frequency of k-mers to report. In the above example there are 100 samples and only k-mers appearing in between 5 and 95 samples are output. This is equivalent to the recommended 5% minor allele frequency cutoff.
#Since we use maf = 0.01, let's set -s to 0.01 * nb of strains and and -S to nb of strains - -s, as explained here : https://github.com/johnlees/seer/wiki/Usage#count-your-k-mers
nbStrains=`wc -l fsm_files.txt | awk '{print $1}'`
smallS=$(( $nbStrains / 100 ))
bigS=$(( $nbStrains - $smallS ))

/data2/leandro/GWAS/PLOS_review/comparison/SEER/fsm-lite/fsm-lite -v -l fsm_files.txt -t tmp_idx -s $smallS -S $bigS > fsm_kmers.txt
rm tmp_idx.meta tmp_idx.tmp
gzip fsm_kmers.txt
cd ..
