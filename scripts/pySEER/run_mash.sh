set -eux
mkdir mash && cd mash
awk '{print $2}' ../fsmlite/fsm_files.txt > mash_input.txt
awk 'BEGIN {OFS="\t"}{if($2=="0" || $2=="1"){print $1, $1, $2}}' $1 > phenotypes.txt
/data2/leandro/GWAS/PLOS_review/comparison/SEER/mash/mash-Linux64-v2.0/mash sketch -o reference -l mash_input.txt
/data2/leandro/GWAS/PLOS_review/comparison/SEER/mash/mash-Linux64-v2.0/mash dist reference.msh reference.msh | square_mash > all_distances.tsv
cd ..
