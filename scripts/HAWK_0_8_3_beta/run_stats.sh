set -eux
bash /data2/leandro/GWAS/PLOS_review/comparison/HAWK_0_8_3_beta_runs/supplements/get_stats_from_seqs.sh case_abyss.25_49.fa 1
bash /data2/leandro/GWAS/PLOS_review/comparison/HAWK_0_8_3_beta_runs/supplements/get_stats_from_seqs.sh control_abyss.25_49.fa 0
