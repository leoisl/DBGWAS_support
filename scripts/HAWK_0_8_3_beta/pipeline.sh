#usage:
#bash pipeline.sh <DBGWAS_strains_file> 1 1
set -eux
scriptDir=/data2/leandro/GWAS/PLOS_review/comparison/HAWK_0_8_3_beta_runs/scripts_to_run


filename=$(basename $1)
RUN_FIRST_TWO_STEPS=$2
RUN_LAST_TWO_STEPS=$3


if [ $RUN_FIRST_TWO_STEPS -eq 1 ]
then
	mkdir $filename
fi

cd $filename


if [ $RUN_FIRST_TWO_STEPS -eq 1 ]
then
	#create the case and control
	awk '$2==1' $1 > case
	awk '$2==0' $1 > control

	#run the kmer counter
	/usr/bin/time --verbose -o run_kmer_counter_stats bash ${scriptDir}/run_kmer_counter.sh >kmer_counter.txt.o 2>kmer_counter.txt.e

	#run hawk
	/usr/bin/time --verbose -o run_hawk_internal_stats bash ${scriptDir}/run_hawk_internal.sh >hawk_internal.txt.o 2>hawk_internal.txt.e

	#remove temporary unneeded files
	rm -rfv *_kmers_sorted.txt
fi

if [ $RUN_LAST_TWO_STEPS -eq 1 ]
then
	#run abyss
	/usr/bin/time --verbose -o run_abyss_stats bash ${scriptDir}/run_abyss.sh >abyss.txt.o 2>abyss.txt.e

	#get stats on the kmers
	/usr/bin/time --verbose -o run_stats_stats bash ${scriptDir}/run_stats.sh >stats.txt.o 2>stats.txt.e

	#produce the final file
	cat <(echo -e "seq\tpvalue\tTotal_case_count_for_k-mer\tTotal_control_count_for_k-mer\tNumber_of_case_samples_k-mer_is_present_in\tNumber_of_control_samples_k-mer_is_present_in") control_abyss.25_49.fa.seqs_stats case_abyss.25_49.fa.seqs_stats > hawk_out_on_$filename
fi

cd ..
