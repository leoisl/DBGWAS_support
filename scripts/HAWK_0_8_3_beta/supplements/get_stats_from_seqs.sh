#usage: script case_abyss.25_49.fa 1/0
#the second argument is an int
#1 == case
#0 == control
set -eux

echo -n "" > ${1}.seqs_stats
awk 'NR%2==0' $1 > ${1}.seqs_only
while read seq; do
        stats=$(bash /data2/leandro/GWAS/PLOS_review/comparison/HAWK_0_8_3_beta_runs/supplements/DBGWASrunKmerSummary $seq ${2})
	echo "${seq},${stats}" >> ${1}.seqs_stats
done <${1}.seqs_only

sed -i -e "s/,/\t/g" ${1}.seqs_stats
